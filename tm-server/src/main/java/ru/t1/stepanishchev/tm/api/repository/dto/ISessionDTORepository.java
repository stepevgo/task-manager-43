package ru.t1.stepanishchev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {

    @Nullable
    SessionDTO findOneById(@NotNull String id);

}