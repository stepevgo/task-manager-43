package ru.t1.stepanishchev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

    @NotNull
    Session create(@Nullable Session session);

    Boolean existsById(@Nullable String id);

    @Nullable
    Session findOneById(@Nullable String id);

    @NotNull
    Session removeById(@Nullable String id);

}