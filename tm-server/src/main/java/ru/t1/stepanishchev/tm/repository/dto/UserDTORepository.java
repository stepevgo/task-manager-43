package ru.t1.stepanishchev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.dto.IUserDTORepository;
import ru.t1.stepanishchev.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager.createQuery("FROM UserDTO", UserDTO.class).getResultList();
    }

    @Override
    @Nullable
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public UserDTO findOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return findOneByLogin(login) != null;
    }

    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return findOneByLogin(email) != null;
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM UserDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

}