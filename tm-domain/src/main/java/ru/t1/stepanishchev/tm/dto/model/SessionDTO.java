package ru.t1.stepanishchev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_session")
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

}